const moveUserIcon = document.querySelector(".move.user");
const optionsWrapper = document.querySelector(".options");
let score = [0, 0];

optionsWrapper.addEventListener("click", handleOptionClick);


function handleOptionClick(event) {
    if (event.target.className == "option") {
        const icons = ["Rock.png", "Paper.png", "Scissors.png"];
        changeUserBackground(event)
    
        let userChoice = event.target.id.toLowerCase();
        let computerChoice = computerResponse(icons);
    
        let moveResult = calcResult(userChoice, computerChoice)
        // Без цикла for никак реализовать не получалось (в случае с forEach параметры передаются по значению
        // и не меняются, тогда как с циклом for по ссылке)
        for (let i = 0; i < score.length; i++) {
            score[i] += moveResult[i]
        }
        rewriteScore(score)
        changeNotification(moveResult)
    }
}


function changeUserBackground(event) {
    moveUserIcon.style.backgroundImage = `url(${event.target.src})`;
    moveUserIcon.style.backgroundSize = "104%";
}


function computerResponse(icons) {
    const moveComputerIcon = document.querySelector(".move.computer");
    let randomIcon = icons[Math.floor(Math.random() * icons.length)];

    moveComputerIcon.style.backgroundImage = `url(images/${randomIcon})`;
    moveComputerIcon.style.backgroundSize = "104%";

    return randomIcon.replace(".png", "").toLowerCase();
}


function calcResult(userChoice, computerChoice) {
    
    const options = ["rock", "paper", "scissors"];
    let result = [0, 0];

    switch (userChoice) {
        case options[0]:
            switch (computerChoice) {
                case options[1]:
                    result[1]++;
                    break;
                case options[2]:
                    result[0]++;
                    break
            }
            break
        case options[1]:
            switch (computerChoice) {
                case options[0]:
                    result[0]++;
                    break;
                case options[2]:
                    result[1]++;
                    break;
            }
            break
        case options[2]:
            switch (computerChoice) {
                case options[0]:
                    result[1]++;
                    break;
                case options[1]:
                    result[0]++;
                    break;
            }
            break
        }
    return result
    }

function rewriteScore(score) {
    const userScore = document.querySelector(".score.user")
    const computerScore = document.querySelector(".score.computer")
    userScore.innerText = `You: ${score[0]}`
    computerScore.innerText = `Computer: ${score[1]}`
}

function changeNotification(result) {
    const notice = document.querySelector(".notification");
    if (result[0] == 0 && result[1] != 0) {
        notice.innerText = "You lose("
    }
    else if (result[1] == 0 && result[0] != 0){
        notice.innerText = "You win!"
    }
    else {
        notice.innerText = "Draw"
    }
}